import React, { Component } from 'react'

class FooterComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                 
        }
    }

    render() {
        return (
            <div>
                <footer id="footer">
                    <div className="center">
                        <p>
                            &copy; Gestión de usuarios 2021
                        </p>
                    </div>
                </footer>
            </div>
        )
    }
}

export default FooterComponent