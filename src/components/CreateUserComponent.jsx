import React, { Component } from 'react'
import UserService from '../services/UserService'

/*Componente para crear un nuevo usuario y actualizarlo.*/ 
class CreateUserComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            userName: '',
            firstName: '',
            lastName: '',
            email: '',
            password: ''
        }

        this.changeUserNameHandler = this.changeUserNameHandler.bind(this);
        this.changeFirstNameHandler = this.changeFirstNameHandler.bind(this);
        this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
        this.changeEmailHandler = this.changeEmailHandler.bind(this);
        this.changePasswordHandler = this.changePasswordHandler.bind(this);
        this.saveOrUpdateUser = this.saveOrUpdateUser.bind(this);
    }

    componentDidMount(){

        if(this.state.id === '_add'){
            return
        }else{
            UserService.getUserById(this.state.id).then( (res) =>{
                let user = res.data;
                this.setState({firstName: user.firstName,
                    lastName: user.lastName,
                    email : user.email
                });
            });
        }        
    }
    saveOrUpdateUser = (e) => {
        e.preventDefault();
        let user = {userName: this.state.userName,firstName: this.state.firstName, lastName: this.state.lastName, email: this.state.email, password: this.state.password};
        console.log('user => ' + JSON.stringify(user));

        if(this.state.id === '_add'){
            UserService.createUser('11ef0da3e0d571ef0b33f3b8ceb7df39','core_user_create_users','json', this.state.userName, this.state.firstName, this.state.lastName, this.state.email, this.state.password).then(res =>{
                console.log(res.data.errorcode);
                console.log(res);
                
                if(res.data.exception!=undefined) {
                    alert(res.data.message);
                } else {
                    this.props.history.push('/users');
                }
            });
        }else{
            UserService.updateUser('11ef0da3e0d571ef0b33f3b8ceb7df39','core_user_update_users','json', this.state.userName, this.state.firstName, this.state.lastName, this.state.email, this.state.password, this.state.id).then( res => {
                console.log(res);
                this.props.history.push('/users');
            });
        }
    }
    
    changeUserNameHandler= (event) => {
        this.setState({userName: event.target.value});
    }

    changeFirstNameHandler= (event) => {
        this.setState({firstName: event.target.value});
    }

    changeLastNameHandler= (event) => {
        this.setState({lastName: event.target.value});
    }

    changeEmailHandler= (event) => {
        this.setState({email: event.target.value});
    }

    changePasswordHandler= (event) => {
        this.setState({password: event.target.value});
    }

    cancel(){
        this.props.history.push('/users');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center encabezado">Nuevo usuario</h3>
        }else{
            return <h3 className="text-center encabezado">Actualizar usuario</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3 recuadro">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                    <div className = "form-group">
                                            <label className="form-labels"> Usuario: </label>
                                            <input placeholder="Nombre de usuario" name="userName" className="form-control" 
                                                value={this.state.userName} onChange={this.changeUserNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label className="form-labels"> Nombre(s): </label>
                                            <input placeholder="Nombres" name="firstName" className="form-control" 
                                                value={this.state.firstName} onChange={this.changeFirstNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label className="form-labels"> Apellidos: </label>
                                            <input placeholder="Apllidos" name="lastName" className="form-control" 
                                                value={this.state.lastName} onChange={this.changeLastNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label className="form-labels"> Email: </label>
                                            <input placeholder="Email" name="email" className="form-control" 
                                                value={this.state.email} onChange={this.changeEmailHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label className="form-labels"> Password: </label>
                                            <input type="password" placeholder="Password" name="password" className="form-control" 
                                                value={this.state.password} onChange={this.changePasswordHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveOrUpdateUser}>Guardar</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancelar</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default CreateUserComponent