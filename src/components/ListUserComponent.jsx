import React, { Component } from 'react'
import UserService from '../services/UserService'

/*Componente que enlista todos los usuarios.*/ 
class ListUserComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                users: []
        }
        this.addUser = this.addUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
    }

    deleteUser(id){
        UserService.deleteUser('11ef0da3e0d571ef0b33f3b8ceb7df39','core_user_delete_users','json', id).then( res => {
            alert("El usuario se ha eliminado correctamente.");
            console.log(res);
            this.setState({users: this.state.users.filter(user => user.id !== id)});
        });
    }

    viewUser(id){
        this.props.history.push(`/view-user/${id}`);
    }
    
    editUser(id){
        this.props.history.push(`/add-user/${id}`);
    }

    componentDidMount(){
        UserService.getUsers('11ef0da3e0d571ef0b33f3b8ceb7df39','tool_dataprivacy_get_users','json','0').then((res) => {
            this.setState({ users: res.data});
        });
    }


    addUser(){
        this.props.history.push('/add-user/_add');
    }

    render() {
        return (
            <div>
                 <h2 className="text-center encabezado">Usuarios</h2>
                 <div className = "row">
                    <button className="btn btn-info" onClick={this.addUser}>Nuevo usuario</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead className="table-headers">
                                <tr>
                                    <th> Nombre completo</th>
                                    <th> Email</th>
                                    <th> Acciones</th>
                                </tr>
                            </thead>
                            <tbody className="table-body-label">
                                {
                                    this.state.users.map(
                                        user => 
                                        <tr key = {user.id} id={user.id}>
                                             <td> {user.fullname} </td>
                                             <td> {user.extrafields[0].value} </td>   
                                             <td>
                                                 <button onClick={ () => this.editUser(user.id)} className="btn btn-info">Actualizar </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteUser(user.id)} className="btn btn-danger">Eliminar </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewUser(user.id)} className="btn btn-info">Ver detalle </button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListUserComponent