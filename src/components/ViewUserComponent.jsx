import React, { Component } from 'react'
import UserService from '../services/UserService'

/*Componente que muestra el detalle de los usuarios.*/ 
class ViewUserComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            user: {}
        }
    }

    componentDidMount(){
        UserService.getUserById('11ef0da3e0d571ef0b33f3b8ceb7df39','core_user_get_users','json', this.state.id).then( res => {
            this.setState({user: res.data.users[0]});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3 recuadro">
                    <h3 className = "text-center encabezado"> Detalle del usuario</h3>
                    <div className = "card-body">
                    { console.log(this.state.id) } 
                            <div className = "row center">
                                <img src={ this.state.user.profileimageurl } className="imagen-usuario" alt="Imagen Usuario" title="Imagen Usuario" />
                            </div>

                            <div className = "row center">
                                <label className="labels-detalle-usuario"> <strong > Usuario: </strong> </label>
                                <div> { this.state.user.username }</div>
                            </div>

                            <div className = "row center">
                                <label className="labels-detalle-usuario"> <strong>Nombre(s):</strong> </label>
                                <div> { this.state.user.firstname }</div>
                            </div>

                            <div className = "row center">
                                <label className="labels-detalle-usuario"> <strong>Apellido:</strong> </label>
                                <div> { this.state.user.lastname }</div>
                            </div>

                            <div className = "row center">
                                <label className="labels-detalle-usuario"> <strong>Email:</strong> </label>
                                <div> { this.state.user.email }</div>
                            </div>

                    </div>

                </div>
            </div>
        )
    }
}

export default ViewUserComponent