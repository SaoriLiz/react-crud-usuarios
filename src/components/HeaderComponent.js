import React, { Component } from 'react'

class HeaderComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                 
        }
    }

    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                            <span>
                                <strong className="titulo">Gestión de <label className="subTitulo"> usuarios </label> </strong>
                            </span>
                    </nav>
                </header>
            </div>
        )
    }
}

export default HeaderComponent