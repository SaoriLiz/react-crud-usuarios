import axios from 'axios';
/*Servicios del api*/ 

const USER_API_BASE_URL = "http://local.hmartepost.com/moodle38/webservice/rest/server.php?";

class UserService {

    /*Devuelve la lista de los usuarios.*/
    getUsers(wstoken, wsfunction, moodlewsrestformat, query){
        return axios.get(USER_API_BASE_URL + 'wstoken=' + wstoken + '&wsfunction=' + wsfunction + '&moodlewsrestformat=' + moodlewsrestformat + '&query=' + query);
    }

    /*Crea un usuario.*/
    createUser(wstoken, wsfunction, moodlewsrestformat, username, firstname, lastname, email, password){ 
        return axios.post(USER_API_BASE_URL + 'wstoken=' + wstoken + '&wsfunction=' + wsfunction + '&moodlewsrestformat=' + moodlewsrestformat + '&users[0][username]=' + username + '&users[0][firstname]=' + firstname + '&users[0][lastname]=' + lastname + '&users[0][email]=' + email + '&users[0][password]=' + password);
    }

    /*Obtiene el detalle de un usuario mediant el id.*/
    getUserById(wstoken, wsfunction, moodlewsrestformat, id){
        return axios.get(USER_API_BASE_URL + 'wstoken=' + wstoken + '&wsfunction=' + wsfunction + '&moodlewsrestformat=' + moodlewsrestformat + '&criteria[0][key]=id' + '&criteria[0][value]=' + id);
    }

    /*Actualiza un usuario mediante su id.*/
    updateUser(wstoken, wsfunction, moodlewsrestformat, username, firstname, lastname, email, password, id){
        return axios.post(USER_API_BASE_URL + 'wstoken=' + wstoken + '&wsfunction=' + wsfunction + '&moodlewsrestformat=' + moodlewsrestformat + '&users[0][username]=' + username + '&users[0][firstname]=' + firstname + '&users[0][lastname]=' + lastname + '&users[0][email]=' + email + '&users[0][password]=' + password + '&users[0][id]=' + id);
    }

    /*Elimina un usuario mediante su id.*/
    deleteUser(wstoken, wsfunction, moodlewsrestformat, id){
        return axios.post(USER_API_BASE_URL + 'wstoken=' + wstoken + '&wsfunction=' + wsfunction + '&moodlewsrestformat=' + moodlewsrestformat + '&userids[0]=' +id);
    }
}

export default new UserService()